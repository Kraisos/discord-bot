const Discord = require("discord.js");

module.exports = {
  name: "test",
  description: "Tests",
  cooldown: 1,
  execute(message) {
    message.delete();

    // Create the attachment using MessageAttachment
    const attachment = new Discord.MessageAttachment("./wallpaper.jpg");
    // Send the attachment in the message channel
    message.channel.send(attachment);

    message.reply(message.author.displayAvatarURL());
  },
};
